import math
import os
import sys
import time
from functools import partial

import matplotlib.pyplot as plt
import numpy as np
from PIL import Image
from PIL.ImageQt import ImageQt
from PyQt5.QtCore import Qt, QSignalMapper
from PyQt5.QtGui import QImage, QPixmap, QColor
from PyQt5.QtWidgets import QApplication, QHBoxLayout, QWidget
from PyQt5.QtWidgets import QSlider, QLabel, QVBoxLayout, QDialog, QMessageBox, QMainWindow, QAction, \
    QFileDialog, QInputDialog
from scipy.stats import truncnorm

from Border import Border
from Filter import Filter
from MatrixHelper import MatrixHelper
from NoiseGenerator import NoiseGenerator
from BandWindow import BandWindow
from Displayer import Displayer
from FormWindow import FormWindow
from ImageViewer import ImageViewer
from SelectAreaWindow import SelectAreaWindow
from SequenceWindow import SequenceWindow


class Window(QMainWindow):

    def __init__(self):
        super(Window, self).__init__()

        # Arbitrary values
        self.width = 1200
        self.height = 800
        self.imageViewerWidth = 400
        self.imageViewerHeight = 400
        self.setGeometry(50, 50, self.width, self.height)
        self.setWindowTitle('ATI')
        self.init_menu()
        self.displayer = Displayer()
        self.image_widget = QWidget()
        self.image_widget.setLayout(self.init_layout())
        self.setCentralWidget(self.image_widget)

        self.matrix_helper = MatrixHelper()
        self.filter_helper = Filter(self.matrix_helper)
        self.border_helper = Border(self.matrix_helper, self.filter_helper)
        self.noise_generator = NoiseGenerator(self.matrix_helper)

        placeholder = QPixmap(self.imageViewerWidth, self.imageViewerHeight)
        placeholder.fill(QColor("gray"))
        img = placeholder.toImage()
        self.displayer.set_img('l', img=img)
        self.displayer.set_img('r', img=img.copy())
        self.init_text()
        self.displayer.get_viewer('l').set_output_label(self.outputText)
        self.displayer.get_viewer('r').set_output_label(self.outputText)
        self.show()
        self._raw_sizes = {'BARCO.RAW': (290, 207), 'GIRL.RAW': (389, 164), 'GIRL2.RAW': (256, 256),
                           'LENA.RAW': (256, 256), 'FRACTAL.RAW': (200, 200)}

    def init_layout(self):
        h_layout = QHBoxLayout(self)
        left_image_viewer = ImageViewer(self, self.width/4, (self.height - self.imageViewerHeight)/2, self.imageViewerWidth, self.imageViewerHeight)
        right_image_viewer = ImageViewer(self, 3*self.width/4, (self.height - self.imageViewerHeight)/2, self.imageViewerWidth, self.imageViewerHeight)
        self.displayer.add_viewer(left_image_viewer, 'l')
        self.displayer.add_viewer(right_image_viewer, 'r')
        h_layout.addWidget(left_image_viewer)
        h_layout.addStretch()
        h_layout.addWidget(right_image_viewer)
        return h_layout


    def init_text(self, text = ''):
        self.outputText = QLabel(self)
        self.outputText.setGeometry(self.width/4, self.height*2/3, self.width/2, self.height/4)
        self.outputText.setAlignment(Qt.AlignCenter)
        self.outputText.setText(text)
        font = self.outputText.font()
        font.setPointSize(24)
        font.setBold(True)
        self.outputText.setFont(font)
        self.outputText.show()

    def display_text(self, text = ''):
        self.outputText.setText(text)

    def mousePressEvent(self, event):
        if event.button() == Qt.LeftButton:
            self.displayer.get_viewer('l').resetSelectedArea()
            self.displayer.get_viewer('r').resetSelectedArea()

    # Initializes the upper toolbar, assigning shortcuts and actions to the buttons.
    def init_menu(self):
        self.statusBar()
        self.mainMenu = self.menuBar()
        self.mapper = QSignalMapper(self)
        fileMenu = self.mainMenu.addMenu('&Archivo')
        fileMenu.addAction('Cargar', self.load_image_wrapper)
        fileMenu.addAction('Cargar Solo Izq', self.load_left_image_wrapper)
        fileMenu.addAction('Cargar Solo Der', self.load_right_image_wrapper)
        fileMenu.addAction('Guardar', self.save_image)

        createMenu = self.mainMenu.addMenu('&Crear')
        createMenu.addAction('Crear circulo', self.create_circle)
        createMenu.addAction('Crear cuadrado', self.create_square)
        createMenu.addAction('Crear degradee grises', self.create_gray_degradee)
        createMenu.addAction('Crear degradee colores', self.create_colored_degradee)
        createMenu.addAction('Histograma', self.histogram)

        op_menu = self.mainMenu.addMenu('&Operaciones')
        op_menu.addAction('Copiar seleccion', self.copy_selection)
        op_menu.addAction('Pintar seleccion', self.paint_selection)
        op_menu.addAction('Info de seleccion', self.selection_info)
        op_menu.addAction('Ver Bandas', self.show_bands)
        op_menu.addAction('Resta (Der - Izq)', self.substract)
        op_menu.addAction('Resta (Izq - Der)', self.substract_inv)
        op_menu.addAction('Producto', self.product_wrapper)
        op_menu.addAction('Rango Dinamico', self.dyn_range_wrapper)
        op_menu.addAction('Potencia Gamma', self.gamma_exp_wrapper)
        op_menu.addAction('Negativo', self.negative)
        op_menu.addAction('Ecualizar', self.equalize)
        op_menu.addAction('Contraste', self.contrast_wrapper)
        op_menu.addAction('Umbral', self.umbral_wrapper)
        op_menu.addAction('Umbral Global', self.global_threshold)
        op_menu.addAction('Umbral de Otsu', self.otsu_threshold)
        op_menu.addAction('Umbral de Otsu RGB', self.otsu_rgb_threshold)
        op_menu.addAction('Escala de grises', self.to_grayscale)
        op_menu.addAction('Copiar izq a der', self.copy_left_to_right)
        op_menu.addAction('Copiar der a izq', self.copy_right_to_left)

        noise_menu = self.mainMenu.addMenu('&Ruido')
        noise_menu.addAction('Samples de ruido', self.generate_noise_samples)
        noise_menu.addAction('Ruido Normal', partial(self.apply_noise, 'normal'))
        noise_menu.addAction('Ruido de Rayleigh', partial(self.apply_noise, 'rayleigh'))
        noise_menu.addAction('Ruido exponencial', partial(self.apply_noise, 'exp'))
        noise_menu.addAction('Sal y pimienta', self.salt_and_pepper_input)

        filter_menu = self.mainMenu.addMenu('&Filtros')
        filter_menu.addAction('Media', self.mean_wrapper)
        filter_menu.addAction('Mediana', self.median_wrapper)
        filter_menu.addAction('Mediana Ponderada', self.weighted_median_wrapper)
        filter_menu.addAction('Pasa-altos', self.pass_high_filter)
        filter_menu.addAction('Gaussiano', self.gauss_filter_wrapper)
        filter_menu.addAction('Isotropico', self.isotropic_filter_wrapper)
        filter_menu.addAction('Anisotropico', self.anisotropic_filter_wrapper)
        filter_menu.addAction('Filtro Bilateral', self.bilateral_filter)

        border_menu = self.mainMenu.addMenu('&Bordes')
        border_menu.addAction('Prewitt', self.prewitt_border)
        border_menu.addAction('Sobel', self.sobel_border)
        border_menu.addAction('Laplaciano', self.laplacian_border)
        border_menu.addAction('Canny', self.canny_edge)
        border_menu.addAction('Susan', self.susan_edge)
        border_menu.addAction('Hough', self.hough_transform)
        border_menu.addAction('Contornos Activos', self.active_contour)
        border_menu.addAction('Contornos Activos (Video)', self.active_contour_sequence)
        border_menu.addAction('Detector de Harris', self.harris_detector)
        border_menu.addAction('SIFT', self.sift)

    def load_image_wrapper(self):
        self.load_image()

    def load_left_image_wrapper(self):
        img = self.openImgFromFile()
        if img is not None:
            self.displayer.set_img('l', img=img)

    def load_right_image_wrapper(self):
        img = self.openImgFromFile()
        if img is not None:
            self.displayer.set_img('r', img=img)

    def load_image(self, fileName=None):
        img = self.openImgFromFile(fileName)
        if img is not None:
            self.displayer.set_img('l', img=img)
            self.displayer.set_img('r', img=img.copy())
    
    def save_image(self):
        options = QFileDialog.Options()
        fileName, _ = QFileDialog.getSaveFileName(self, 'asd', '',
                                               'Images (*.png *.jpeg *.jpg *.bmp *.gif *.raw *.pgm *.ppm)', options=options)
        if os.path.basename(fileName) != '':
            self.displayer.get_viewer('r').pixmap().save(fileName)
        else:
            QMessageBox.information(self, "Image Viewer", "Please specify a filename.")

    def paint_selection(self):
        self.displayer.paint_selection('r')

    def copy_selection(self):
        self.displayer.copy_selection('l', 'r')

    def copy_left_to_right(self):
        self.displayer.copy('l', 'r')

    def copy_right_to_left(self):
        self.displayer.copy('r', 'l')

    def create_circle(self):
        width = 200
        height = 200
        r = 50
        mtx = np.zeros((height, width), dtype='int8')
        for i in range(height):
            for j in range(width):
                if math.sqrt((i - height//2)**2 + (j - width//2)**2) <= r:
                    mtx[i][j] = 255

        self.displayer.set_img('l', mtx=mtx)
        self.displayer.set_img('r', mtx=mtx.copy())

    def selection_info(self):
        if self.displayer.get_viewer('l').isAreaSelected:
            viewer = self.displayer.get_viewer('l')
        if self.displayer.get_viewer('r').isAreaSelected:
            viewer = self.displayer.get_viewer('r')
        viewer.selection_info()
    
    def create_square(self):
        width = 200
        height = 200
        l = 100
        mtx = np.zeros((height, width), dtype='int8')
        for i in range(height):
            for j in range(width):
                if abs(i - height//2) <= l//2 and abs(j - width//2) <= l//2:
                    mtx[i][j] = 255

        self.displayer.set_img('l', mtx=mtx)
        self.displayer.set_img('r', mtx=mtx.copy())
    
    def create_gray_degradee(self):
        width = 256
        height = 200
        mtx = np.zeros((height, width), dtype='int8')
        for i in range(height):
            for j in range(width):
                    mtx[i][j] = (j * 256) // width

        self.displayer.set_img('l', mtx=mtx)
        self.displayer.set_img('r', mtx=mtx.copy())
    
    def create_colored_degradee(self):
        width = 256
        height = 200
        mtx = np.zeros((height, width), dtype=np.uint32)
        color = QColor()
        for j in range(width):
            color.setHsvF(j / width, 1, 1)
            for i in range(height):
                mtx[i][j] = color.rgb()

        self.displayer.set_img('l', mtx=mtx)
        self.displayer.set_img('r', mtx=mtx.copy())

    def show_bands(self):
        img = self.displayer.get_viewer('l').image
        bands = np.zeros((6, img.height(), img.width()), dtype=np.uint8)
        for j in range(img.width()):
            for i in range(img.height()):
                color = img.pixelColor(j, i)
                bands[0][i][j] = color.red()
                bands[1][i][j] = color.green()
                bands[2][i][j] = color.blue()
                bands[3][i][j] = color.hue()
                bands[4][i][j] = color.saturation()
                bands[5][i][j] = color.value()

        BandWindow(self, bands, img.width(), img.height())

    def product_wrapper(self):
        scalar, ok_pressed = QInputDialog.getDouble(self, "Ingrese el escalar", "Escalar:", 0, 0, 100, 2)
        if ok_pressed:
            mtx = self.displayer.image_to_matrix('r')
            mtx = self.matrix_helper.product(scalar, mtx)
            self.displayer.display_mtx(mtx, 'Producto')

    def dyn_range_wrapper(self):
        mtx = self.displayer.image_to_matrix('r')
        mtx = self.matrix_helper.dynamic_range(mtx)
        self.displayer.display_mtx(mtx)

    def gamma_exp_wrapper(self):
        gamma, ok_pressed = QInputDialog.getDouble(self, "Ingrese gamma", "Gamma:", 1, 0, 100, 2)
        if ok_pressed:
            mtx = self.displayer.image_to_matrix('r')
            mtx = self.matrix_helper.gamma_exp(mtx, gamma)
            self.displayer.display_mtx(mtx)

    def contrast_wrapper(self):
        fields = {'r1': 'int', 'r2': 'int'}
        values, ok_pressed = FormWindow.get_values(self, "Ingrese los valores", fields)
        if ok_pressed:
            mtx = self.displayer.image_to_matrix('r')
            mtx = self.matrix_helper.contrast(mtx, values['r1'], values['r2'])
            self.displayer.display_mtx(mtx, name='Contraste')

    def umbral_wrapper(self):
        umbral, ok_pressed = QInputDialog.getInt(self, "Ingrese el umbral", "Umbral:", 0, 0, 255)
        if ok_pressed:
            self.normal_threshold(umbral)

    def mean_wrapper(self):
        size, ok_pressed = QInputDialog.getInt(self, "Ingrese el tamaño del filtro", "Tamaño:", 1, 0, 33, step=2)
        if not ok_pressed:
            return
        filter = np.ones((size, size))
        filter /= np.sum(filter)
        mtx = self.displayer.image_to_matrix('r')
        mtx = self.filter_helper.apply_mean_filter(mtx, filter)
        self.displayer.display_mtx(mtx, name='Filtro de la media de tamaño ' + str(size))

    def median_wrapper(self):
        size, ok_pressed = QInputDialog.getInt(self, "Ingrese el tamaño del filtro", "Tamaño:", 1, 0, 33, step=2)
        if not ok_pressed:
            return
        filter = np.ones((size, size))
        mtx = self.displayer.image_to_matrix('r')
        mtx = self.filter_helper.apply_median_filter(mtx, filter)
        self.displayer.display_mtx(mtx, name='Filtro de la mediana de tamaño ' + str(size))

    def weighted_median_wrapper(self):
        filter = np.ones((3, 3), dtype=np.int32)
        filter[0, 1] = 2
        filter[1, 0] = 2
        filter[1, 2] = 2
        filter[2, 1] = 2
        filter[1, 1] = 4
        mtx = self.displayer.image_to_matrix('r')
        mtx = self.filter_helper.apply_weighted_median_filter(mtx, filter)
        self.displayer.display_mtx(mtx, name='Filtro de la mediana pesado de tamaño 3')

    def pass_high_filter(self):
        filter = np.zeros((3, 3)) - 1
        filter[1, 1] = 8
        filter /= 9
        mtx = self.displayer.image_to_matrix('r')
        mtx = self.filter_helper.apply_mean_filter(mtx, filter, normalize=True)
        self.displayer.display_mtx(mtx, name='Filtro pasa altos')

    def gauss_filter_wrapper(self):
        fields = {'Tamaño': 'int', 'Desvio': 'float'}
        values, ok_pressed = FormWindow.get_values(self, "Ingrese los valores", fields)
        if not ok_pressed:
            return
        size = values['Tamaño']
        std = values['Desvio']
        if size % 2 == 1:
            mtx = self.displayer.image_to_matrix('r')
            mtx = self.filter_helper.apply_gauss_filter(mtx, size, std)
            self.displayer.display_mtx(mtx, name='Filtro gaussiano de tamaño {:d} y sigma {:.2f}'.format(size, std))

    def isotropic_filter_wrapper(self):
        times, ok_pressed = QInputDialog.getInt(self, "Ingrese la cantidad de veces", "Veces:", 1, 1, 255)
        if not ok_pressed:
            return
        mtx = self.displayer.image_to_matrix('r')
        mtx = mtx.astype(dtype=np.float)
        mtx = self.filter_helper.apply_fast_diffusion_filter(mtx, 'isotropic', times)
        self.displayer.display_mtx(mtx, name='Filtro isotropico {:d} iteraciones'.format(times))

    def anisotropic_filter_wrapper(self):
        fields = {'Detector': 'combo', 'Desvio': 'float', 'Veces': 'int'}
        values, ok_pressed = FormWindow.get_values(self, "Ingrese los valores", fields, valid_values={'Detector': ['leclerc', 'lorentziano']})
        if not ok_pressed:
            return
        detector = values['Detector']
        sigma = values['Desvio']
        times = values['Veces']
        mtx = self.displayer.image_to_matrix('r')
        mtx = mtx.astype(dtype=np.float)
        mtx = self.filter_helper.apply_fast_diffusion_filter(mtx, detector, times, sigma)
        self.displayer.display_mtx(mtx, name='Filtro anisotropico {:d} iteraciones con {:s} y sigma {:.2f}'.format(times, detector, sigma))

    def bilateral_filter(self):
        values, ok_pressed = FormWindow.get_values(self, "Ingrese los valores", {'sigma r':'float', 'sigma s': 'float'})
        if not ok_pressed:
            return

        sigma_s = values['sigma s']
        sigma_r = values['sigma r']
        mtx = self.displayer.image_to_matrix('r')
        mtx = self.filter_helper.apply_bilateral_filter(mtx, sigma_s=sigma_s, sigma_r=sigma_r)
        self.displayer.display_mtx(mtx, name='Bilateral con sigma s: {:.2f} y sigma r: {:.2f}'.format(sigma_s, sigma_r))

    def prewitt_border(self):
        v_filter = np.array([[-1,-1,-1], [0, 0, 0], [1,1,1]])
        values, ok_pressed = FormWindow.get_values(self, "Ingrese los valores", {'rotaciones':'int', 'combinacion': 'combo'}, valid_values={'combinacion': ['distancia', 'max', 'min', 'promedio']})
        if not ok_pressed:
            pass
        mtx = self.displayer.image_to_matrix('r')
        mtx = self.matrix_helper.gradient_edge(mtx, values['combinacion'], v_filter, rotations=values['rotaciones'])
        self.displayer.display_mtx(mtx, name='Prewitt con {:s}'.format(values['combinacion']))

    def sobel_border(self):
        v_filter = np.array([[-1,-2,-1], [0, 0, 0], [1,2,1]])
        values, ok_pressed = FormWindow.get_values(self, "Ingrese los valores", {'rotaciones':'int', 'combinacion': 'combo'}, valid_values={'combinacion': ['distancia', 'max', 'min', 'promedio']})
        if not ok_pressed:
            pass
        mtx = self.displayer.image_to_matrix('r')
        mtx = self.matrix_helper.gradient_edge(mtx, values['combinacion'], v_filter, rotations=values['rotaciones'])
        self.displayer.display_mtx(mtx, name='Sobel con {:s}'.format(values['combinacion']))

    def laplacian_border(self):
        filter = np.array([[0,-1,0], [-1,4,-1], [0,-1,0]])
        values, ok_pressed = FormWindow.get_values(self, "Ingrese los valores", {'combinacion': 'combo'}, valid_values={'combinacion': ['distancia', 'max', 'min', 'promedio']})
        if not ok_pressed:
            pass
        mtx = self.displayer.image_to_matrix('r')
        mtx = self.matrix_helper.gradient_edge(mtx, values['combinacion'], filter, rotations=1)
        self.displayer.display_mtx(mtx, name='Laplaciano con {:s}'.format(values['combinacion']))

    def canny_edge(self):
        values, ok_pressed = FormWindow.get_values(self, "Ingrese los valores", {'std': 'float'})
        if not ok_pressed:
            pass
        mtx = self.displayer.image_to_matrix('r')
        mtx = self.border_helper.canny_edge(mtx, values['std'])
        self.displayer.display_mtx(mtx, name='Canny')

    def susan_edge(self):
        mtx = self.displayer.image_to_matrix('r')
        mtx = self.border_helper.susan(mtx)
        self.displayer.display_mtx(mtx, name='Susan')

    def hough_transform(self):
        values, ok_pressed = FormWindow.get_values(self, "Ingrese los valores", {'error': 'float', 'candidatos': 'int'})
        if not ok_pressed:
            pass
        mtx = self.displayer.image_to_matrix('r')
        mtx = self.border_helper.hough_transform(mtx, e=values['error'], candidate_count=values['candidatos'])
        self.displayer.display_mtx(mtx, name='Hough')

    def active_contour(self):
        if self.displayer.get_viewer('r').isAreaSelected:
            mtx = self.displayer.image_to_matrix('r').astype(np.int32)
            from_x, from_y, to_x, to_y = self.displayer.get_viewer('r').get_selected_coords()

            phi_mtx = self.border_helper.generate_initial_phi_mtx(mtx, from_x, from_y, to_x, to_y)

            phi_mtx = self.border_helper.active_contour(mtx, phi_mtx)

            mtx[phi_mtx == 1] = [255, 0, 0]
            mtx[phi_mtx == -1] = [0, 0, 255]

            self.displayer.display_mtx(mtx, name='Contornos Activos')

    def active_contour_sequence(self):
        sequence = self.openImageSequence()
        if len(sequence) > 0:
            mtx = sequence[0]
            img = self.displayer.matrix_to_image(mtx)
            area, ok = SelectAreaWindow.getActions(img, mtx.shape[0], mtx.shape[1], 'Seleccione el area')
            if ok:
                from_x, from_y, to_x, to_y = self.area_to_coords(area)

                phi_mtx = self.border_helper.generate_initial_phi_mtx(sequence[0], from_x, from_y, to_x, to_y)

                SequenceWindow.start(sequence, phi_mtx, mtx.shape[0], mtx.shape[1], 'Contornos Activos')



    def area_to_coords(self, area):
        from_x = area.x()
        from_y = area.y()
        to_x = from_x + area.width()
        to_y = from_y + area.height()
        return from_x, from_y, to_x, to_y

    def harris_detector(self):
        fields = {'cim': 'combo', 'K': 'float', 'Tolerance': 'float'}
        values, ok_pressed = FormWindow.get_values(self, "Ingrese los valores", fields, valid_values={'cim': ['cim1', 'cim2', 'cim3']},
                                                   field_order=['cim', 'K', 'Tolerance'])
        if not ok_pressed:
            return
        mtx = self.displayer.image_to_matrix('r')
        mtx = self.border_helper.harris_detector(mtx, k=values['K'], harris_combiner=values['cim'], tolerance=values['Tolerance'])
        self.displayer.display_mtx(mtx, name='Harris')

    def sift(self):
        fields = {'Show Random': 'combo', 'Max Matches': 'int'}
        values, ok_pressed = FormWindow.get_values(self, "Ingrese los valores", fields,
                                                   valid_values={'Show Random': ['True', 'False']},
                                                   field_order=['Max Matches', 'Show Random'])
        if not ok_pressed:
            return
        mtx1 = self.displayer.image_to_matrix('l')
        mtx2 = self.displayer.image_to_matrix('r')

        rand = values['Show Random'] == 'True'
        res = self.border_helper.sift(mtx1, mtx2, max_matches=values['Max Matches'], random=rand)
        self.displayer.display_mtx(res, name='SIFT')

    def negative(self):
        mtx = self.displayer.image_to_matrix('r')
        mtx = 255 - mtx
        self.displayer.set_img('r', mtx=mtx)

    def to_grayscale(self):
        mtx = self.displayer.image_to_matrix('r')
        mtx = self.matrix_helper.rgb2gray(mtx).astype(np.uint8)
        self.displayer.set_img('r', mtx=mtx)

    def histogram(self):
        mtx = self.displayer.image_to_matrix('r')
        mtx = self.matrix_helper.rgb2gray(mtx).astype(np.uint8)
        plt.hist(mtx.flatten(), bins=(np.array(range(256))))
        plt.xlim(0, 255)
        plt.show()

    def equalize(self):
        mtx = self.displayer.image_to_matrix('r')
        mtx = self.matrix_helper.equalize(mtx)
        self.displayer.display_mtx(mtx)

    def apply_threshhold(self, mtx, threshold):
        mtx[mtx >= threshold] = 255
        mtx[mtx < threshold] = 0

        self.displayer.display_mtx(mtx)

    def apply_rgb_threshhold(self, mtx, thresholds):
        for i in range(len(thresholds)):
            mtx[mtx[..., i] >= thresholds[i], i] = 255
            mtx[mtx[..., i] < thresholds[i], i] = 0

        self.displayer.display_mtx(mtx)

    def normal_threshold(self, threshold):
        mtx = self.displayer.image_to_matrix('r').astype(np.float64)
        mtx = self.matrix_helper.rgb2gray(mtx).astype(np.uint8)

        self.apply_threshhold(mtx, threshold)

    def global_threshold(self):
        mtx = self.displayer.image_to_matrix('r').astype(np.float64)
        mtx = self.matrix_helper.rgb2gray(mtx)

        threshold = mtx.mean()

        while True:
            m1 = mtx[mtx >= threshold].mean()
            m2 = mtx[mtx < threshold].mean()
            old_threshold = threshold
            threshold = (m1 + m2) / 2
            if abs(threshold - old_threshold) < 1:
                break

        print("Global: {}".format(threshold))

        self.apply_threshhold(mtx, threshold)

    def otsu_threshold(self):
        mtx = self.displayer.image_to_matrix('r').astype(np.float64)
        mtx = self.matrix_helper.rgb2gray(mtx).astype(np.uint8)

        hist = np.bincount(mtx.flatten(), minlength=256).astype(np.float64)
        probs = hist / hist.sum()
        w_probs = probs * np.array(range(256))
        mg = np.sum(w_probs)

        max_var = -1
        max_ts = []
        for t in range(256):
            p1 = probs[:t+1].sum()
            mt = w_probs[:t+1].sum()
            if 0 < p1 < 1:
                var = (mg * p1 - mt)**2 / (p1 * (1 - p1))

                if var == max_var:
                    max_ts.append(t)
                elif var > max_var:
                    max_var = var
                    max_ts = [t]

        threshold = np.mean(max_ts)

        print("Otsu: {}".format(threshold))

        self.apply_threshhold(mtx, threshold)

    def otsu_rgb_threshold(self):
        mtx = self.displayer.image_to_matrix('r').astype(np.uint8)

        if np.ndim(mtx) == 3:
            thresholds = []
            for i in range(3):
                hist = np.bincount(mtx[..., i].flatten(), minlength=256).astype(np.float64)
                probs = hist / hist.sum()
                w_probs = probs * np.array(range(256))
                mg = np.sum(w_probs)

                max_var = -1
                max_ts = []
                for t in range(256):
                    p1 = probs[:t+1].sum()
                    mt = w_probs[:t+1].sum()
                    if 0 < p1 < 1:
                        var = (mg * p1 - mt)**2 / (p1 * (1 - p1))

                        if var == max_var:
                            max_ts.append(t)
                        elif var > max_var:
                            max_var = var
                            max_ts = [t]

                thresholds.append(np.mean(max_ts))

            print("Otsu_rgb: {}".format(thresholds))

            self.apply_rgb_threshhold(mtx, thresholds)

    def substract(self):
        left = self.displayer.image_to_matrix('l').astype(np.int32)
        right = self.displayer.image_to_matrix('r').astype(np.int32)

        result = right - left
        result = self.matrix_helper.normalize(result)

        self.displayer.display_mtx(result)

    def substract_inv(self):
        left = self.displayer.image_to_matrix('l').astype(np.int32)
        right = self.displayer.image_to_matrix('r').astype(np.int32)

        result = left - right
        result = self.matrix_helper.normalize(result)

        self.displayer.display_mtx(result)

    def generate_noise_samples(self):
        results = self.noise_generator.generate_noise_samples()
        for x in results:
            self.displayer.display_mtx(results[x], name=x)

    def apply_noise(self, noise):
        noise_map = {'normal': ('sigma', self.noise_generator.apply_normal_noise), 'exp': ('lambda', self.noise_generator.apply_exp_noise), 'rayleigh': ('phi', self.noise_generator.apply_rayleigh_noise)}
        value = noise_map[noise]
        values, ok_pressed = FormWindow.get_values(self, 'Ingrese {:s}'.format(value[0]), {value[0]: 'float'})
        if not ok_pressed:
            return
        mtx = self.displayer.image_to_matrix('r')
        mtx = value[1](values, mtx)
        name = noise + ' con ' + value[0] + ':' + str(values[value[0]])
        if values['overwrite']:
            self.displayer.set_img('r', mtx=mtx)
        else:
            self.displayer.display_mtx(mtx, name)

    def salt_and_pepper_input(self):
        label = QLabel()
        label.setText('Parametro p0 (0 a 0.5)')
        slider = QSlider(Qt.Horizontal)
        slider.setMinimum(0)
        slider.setMaximum(500)
        slider.valueChanged[int].connect(self.apply_salt_and_pepper_noise)
        vbox = QVBoxLayout()
        vbox.addWidget(label)
        vbox.addWidget(slider)

        window = QDialog(self)
        window.setLayout(vbox)
        window.setWindowTitle("Salt and Pepper input")
        window.exec()

    def apply_salt_and_pepper_noise(self, value):
        p0 = value / 1000
        
        self.outputText.setText('Sal y pimienta (p0 = {:g})'.format(p0))

        mtx = self.displayer.image_to_matrix('l')
        mtx = self.noise_generator.apply_salt_and_pepper_noise(mtx, p0)

        self.displayer.set_img('r', mtx=mtx)

    def openImgFromFile(self, fileName=None):
        options = QFileDialog.Options()
        print(fileName)
        if fileName == None:
            fileName, _ = QFileDialog.getOpenFileName(self, 'QFileDialog.getOpenFileName()', '',
                                                    'Images (*.png *.jpeg *.jpg *.bmp *.gif *.raw *.pgm *.ppm)', options=options)
        if fileName:
            _, extension = os.path.splitext(fileName)
            if extension.lower() == '.raw':
                img_size = self._raw_sizes[os.path.basename(fileName)]
                with open(fileName, 'rb') as file:
                    img_data = file.read()
                image = ImageQt(Image.frombytes('L', img_size, img_data))
            else:
                image = QImage(fileName)

            if image.isNull():
                QMessageBox.information(self, "Image Viewer", "Cannot load %s." % fileName)
                return

            return image

    def openImageSequence(self):
        options = QFileDialog.Options()

        dirPath = QFileDialog.getExistingDirectory(self, 'Abrir carpeta con secuencia de imagenes', '',
                                            QFileDialog.ShowDirsOnly)

        seq = []
        if dirPath != '':
            os.chdir(dirPath)
            for file in os.listdir(dirPath):
                if os.path.isfile(file):
                    image = QImage(file)
                    mtx = self.displayer.image_to_matrix_lucas(image)
                    seq.append(mtx)

        return seq

app = QApplication(sys.argv)
GUI = Window()
GUI.load_image(fileName='samples/lena-gray.png')
sys.exit(app.exec_())