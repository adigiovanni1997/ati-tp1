from PyQt5.QtCore import Qt
from PyQt5.QtGui import QPixmap, QImage, QColor
from PyQt5.QtGui import QColor
from PyQt5.QtWidgets import QLabel, QWidget, QFileDialog, QRubberBand
from PyQt5.QtCore import QPoint, QRect, QSize, Qt

class ImageViewer(QLabel):
    def __init__(self, parent, posX, posY, width, height):
        super().__init__(parent)

        self.rubberBand = QRubberBand(QRubberBand.Rectangle, self)
        self.origin = QPoint()
        self.left = posX
        self.top = posY
        self.width = width
        self.height = height
        self.move(self.left, self.top)
        self.setMaximumHeight(self.height)
        self.setMaximumWidth(self.width)
        self.isAreaSelected = False
        self.image = None

    def setImage(self, image):
        self.image = image
        pixmap = QPixmap.fromImage(image)
        self.setPixmap(pixmap)
        # self.scaleFactor = 1.0
        #self.setGeometry(self.left - pixmap.width()//2, self.top - pixmap.height()//2, pixmap.width(), pixmap.height())
        self.setFixedSize(pixmap.width(), pixmap.height())
        self.show()

    def setImageScaled(self, image, width, height):
        self.image = image
        pixmap = QPixmap.fromImage(image)
        self.setPixmap(pixmap.scaled(width, height, aspectRatioMode=Qt.KeepAspectRatio))
        self.setGeometry(self.left - pixmap.width() // 2, self.top - pixmap.height() // 2, pixmap.width(),
                         pixmap.height())
        self.setFixedSize(pixmap.width(), pixmap.height())
        self.show()

    def selection_info(self):
        area = self.rubber_band_geometry
        pixel_amount = area.width() * area.height()
        range_x = range(max(0, area.x()), max(area.x() + area.width(), self.width))
        range_y = range(max(0, area.y()), max(area.y() + area.height(), self.height))
        if self.image.isGrayscale():
            accum = 0
            for x in range_x:
                for y in range_y:
                    color = self.image.pixelColor(x, y)
                    gray = (0.21 * color.red() + 0.72 * color.green() + 0.07 * color.blue())
                    accum += gray
            s = 'Pixel amount: {:d}\nGray average: {:f}\n'.format(pixel_amount, accum/pixel_amount)
            self.set_output_text(s)
            return s
        
        red_sum = 0
        green_sum = 0
        blue_sum = 0
        for x in range_x:
            for y in range_y:
                color = self.image.pixelColor(x,y)
                red_sum += color.red()
                green_sum += color.green()
                blue_sum += color.blue()
        s = 'Pixel amount: {:d}\nRed average: {:f}\nGreen average: {:f}\nBlue average: {:f}\n'.format(pixel_amount, red_sum/pixel_amount, green_sum/pixel_amount, blue_sum/pixel_amount)
        self.set_output_text(s)
        return s

    def get_selected_coords(self):
        area = self.rubber_band_geometry
        from_x = area.x()
        from_y = area.y()
        to_x = from_x + area.width()
        to_y = from_y + area.height()
        return from_x, from_y, to_x, to_y

    def set_output_label(self, label):
        self.output_text = label

    def set_output_text(self, text = ''):
        self.output_text.setText(text)

    def mousePressEvent(self, event):       
        if event.button() == Qt.LeftButton:
            self.origin = QPoint(event.pos())
            self.rubberBand.setGeometry(QRect(self.origin, QSize()))
            self.rubberBand.show()
        if event.button() == Qt.RightButton:
            point = QPoint(event.pos())
            rgb = self.image.pixelColor(point.x(), point.y())
            self.set_output_text('Red: {:d}\nGreen: {:d}\nBlue: {:d}\n'.format(rgb.red(), rgb.green(), rgb.blue()))

    def paint_area(self, area, color):
        for x in range(area.x(), area.x() + area.width()):
            for y in range(area.y(), area.y() + area.height()):
                self.image.setPixelColor(x, y, color)
        self.setImage(self.image)

    def get_selected_area(self):
        return self.rubber_band_geometry

    def get_selected_pixmap(self):
        return self.pixmap().copy(self.rubber_band_geometry)

    def resetSelectedArea(self):
        self.isAreaSelected = False
        self.rubberBand.hide()

    def mouseMoveEvent(self, event):
        if not self.origin.isNull():
            self.rubber_band_geometry = QRect(self.origin, event.pos()).normalized()
            self.rubberBand.setGeometry(self.rubber_band_geometry)

    def mouseReleaseEvent(self, event): 
        if event.button() == Qt.LeftButton:
            self.isAreaSelected = True

    def isAreaSelected(self):
        return self.isAreaSelected

    def open(self):
        options = QFileDialog.Options()
        fileName, _ = QFileDialog.getOpenFileName(self, 'QFileDialog.getOpenFileName()', '',
                                                  'Images (*.png *.jpeg *.jpg *.bmp *.gif)', options=options)
        if fileName:
            self.image = QImage(fileName)
            if self.image.isNull():
                QMessageBox.information(self, "Image Viewer", "Cannot load %s." % fileName)
                return

            self.setImage(self.image)