from PyQt5.QtGui import QPixmap, QImage, QColor
import numpy as np
import sys
from PyQt5.QtWidgets import QApplication

class Image():
    def __init__(self, pixmap = None):
        if pixmap is not None:
            channelCount = 4
            img = pixmap.toImage()
            s = img.bits().asstring(img.width() * img.height() * channelCount)
            self.data = np.fromstring(s, dtype=np.uint8).reshape((img.height(), img.width(), channelCount))
            self.width = img.width()
            self.height = img.height()

    def toQImage(self):
        ptr = self.data.tobytes()
        return QImage(ptr, self.width, self.height)


app = QApplication(sys.argv)
placeholder = QPixmap(200, 200)
placeholder.fill(QColor("gray"))
img = Image(placeholder)
qimg = img.toQImage()
print(qimg)