import sys
import engine
import os
import math
import numpy as np
from ImageViewer import ImageViewer
from PyQt5.QtWidgets import QApplication, QColorDialog
from PyQt5 import QtGui
from PyQt5.QtCore import Qt
from PyQt5.QtGui import QImage, QPixmap, QPalette, QPainter, QColor
from PyQt5.QtPrintSupport import QPrintDialog, QPrinter
from PyQt5.QtWidgets import QLabel, QSizePolicy, QScrollArea, QMessageBox, QMainWindow, QMenu, QAction, \
    qApp, QFileDialog
from PIL import Image
from PIL.ImageQt import ImageQt


class BandWindow(QMainWindow):

    def __init__(self, parent, bands, width, height):
        super(BandWindow, self).__init__(parent)

        # Arbitrary values
        self.width = 1200
        self.height = 800
        self.imageViewerWidth = 200
        self.imageViewerHeight = 200
        self.setGeometry(50, 50, self.width, self.height)
        self.setWindowTitle('Band Viewer')

        self.redImageViewer = ImageViewer(self, self.width / 3, self.height / 4,
                                          self.imageViewerWidth, self.imageViewerHeight)
        self.greenImageViewer = ImageViewer(self, self.width / 3, 2 * self.height / 4,
                                            self.imageViewerWidth, self.imageViewerHeight)
        self.blueImageViewer = ImageViewer(self, self.width / 3, 3 * self.height / 4,
                                          self.imageViewerWidth, self.imageViewerHeight)
        self.hueImageViewer = ImageViewer(self, 2 * self.width / 3, self.height / 4,
                                            self.imageViewerWidth, self.imageViewerHeight)
        self.satImageViewer = ImageViewer(self, 2 * self.width / 3, 2 * self.height / 4,
                                          self.imageViewerWidth, self.imageViewerHeight)
        self.valueImageViewer = ImageViewer(self, 2 * self.width / 3, 3 * self.height / 4,
                                            self.imageViewerWidth, self.imageViewerHeight)

        self.redImageViewer.setImageScaled(QImage(bands[0].flatten(), width, height, QImage.Format_Grayscale8), 200, 200)
        self.greenImageViewer.setImageScaled(QImage(bands[1].flatten(), width, height, QImage.Format_Grayscale8), 200, 200)
        self.blueImageViewer.setImageScaled(QImage(bands[2].flatten(), width, height, QImage.Format_Grayscale8), 200, 200)
        self.hueImageViewer.setImageScaled(QImage(bands[3].flatten(), width, height, QImage.Format_Grayscale8), 200, 200)
        self.satImageViewer.setImageScaled(QImage(bands[4].flatten(), width, height, QImage.Format_Grayscale8), 200, 200)
        self.valueImageViewer.setImageScaled(QImage(bands[5].flatten(), width, height, QImage.Format_Grayscale8), 200, 200)
        self.show()
