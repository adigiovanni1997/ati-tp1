import math

import numpy as np


class Filter:

    def __init__(self, matrix_helper):
        self.matrix_helper = matrix_helper

    def gauss_2d(self, x, y, d):
        return math.exp(-(x**2 + y**2) / d**2) / (2 * math.pi * d**2)

    def apply_mean_filter(self, mtx, filter, normalize=False):
        if np.ndim(filter) != 2 or filter.shape[0] != filter.shape[1]:
            pass

        pad = (filter.shape[0] - 1) // 2

        if np.ndim(mtx) == 2:
            padded_mtx = np.pad(mtx, ((pad, pad), (pad, pad)), 'constant', constant_values=0)
        else:
            padded_mtx = np.pad(mtx, ((pad, pad), (pad, pad), (0, 0)), 'constant', constant_values=0)
            filter = np.repeat(filter[:, :, np.newaxis], 3, axis=2)

        result = np.empty_like(mtx)
        for i in range(mtx.shape[0]):
            for j in range(mtx.shape[1]):
                result[i, j, :] = np.sum(padded_mtx[i:i + filter.shape[0], j:j + filter.shape[1], ...] * filter,
                                         axis=(0, 1))

        if normalize:
            result = self.matrix_helper.normalize(result)

        return result

    def apply_median_filter(self, mtx, filter):
        if np.ndim(filter) != 2 or filter.shape[0] != filter.shape[1]:
            pass

        pad = (filter.shape[0] - 1) // 2

        if np.ndim(mtx) == 2:
            padded_mtx = np.pad(mtx, ((pad, pad), (pad, pad)), 'constant', constant_values=0)
        else:
            padded_mtx = np.pad(mtx, ((pad, pad), (pad, pad), (0, 0)), 'constant', constant_values=0)

        result = np.empty_like(mtx)
        for i in range(mtx.shape[0]):
            for j in range(mtx.shape[1]):
                result[i, j, :] = np.median(padded_mtx[i:i + filter.shape[0], j:j + filter.shape[1], ...], axis=(0, 1))

        return result

    def apply_weighted_median_filter(self, mtx, filter):
        if np.ndim(filter) != 2 or filter.shape[0] != filter.shape[1]:
            return

        pad = (filter.shape[0] - 1) // 2

        if np.ndim(mtx) == 2:
            padded_mtx = np.pad(mtx, ((pad, pad), (pad, pad)), 'constant', constant_values=0)
            aux = np.empty((int(filter.sum())))
        else:
            padded_mtx = np.pad(mtx, ((pad, pad), (pad, pad), (0, 0)), 'constant', constant_values=0)
            aux = np.empty((int(filter.sum()), 3))

        result = np.empty_like(mtx)
        for i in range(mtx.shape[0]):
            for j in range(mtx.shape[1]):
                sub_mtx = padded_mtx[i:i + filter.shape[0], j:j + filter.shape[1], ...]
                idx = 0
                for k in range(filter.shape[0]):
                    for l in range(filter.shape[1]):
                        for m in range(filter[k, l]):
                            aux[idx] = sub_mtx[k, l]
                            idx += 1

                result[i, j, :] = np.median(aux, axis=0)

        return result

    def apply_gauss_filter(self, mtx, size, std):
        filter = np.empty((size, size))
        for i in range(size):
            for j in range(size):
                filter[i, j] = self.gauss_2d(i - size//2, j - size//2, std)

        filter /= np.sum(filter)
        return self.apply_mean_filter(mtx, filter)

    def apply_fast_diffusion_filter(self, mtx, detector, times, sigma=None):
        if times <= 0:
            return

        if detector == 'isotropic':
            det_f = lambda x: 1
        elif detector == 'leclerc':
            det_f = lambda x: np.exp(- x*x / sigma**2)
        elif detector == 'lorentziano':
            det_f = lambda x: 1 / (x*x / sigma**2 + 1)
        else:
            return

        pad = 1
        directions = [(0, -1), (0, 1), (-1, 0), (1, 0)]

        for _ in range(times):
            if np.ndim(mtx) == 2:
                padded_mtx = np.pad(mtx, ((pad, pad), (pad, pad)), 'edge')
            else:
                padded_mtx = np.pad(mtx, ((pad, pad), (pad, pad), (0, 0)), 'edge')

            result = np.copy(mtx)
            diffusion = np.zeros_like(mtx)
            for dx, dy in directions:
                grad = padded_mtx[pad+dx:padded_mtx.shape[0]-pad+dx, pad+dy:padded_mtx.shape[1]-pad+dy, :] - mtx
                diffusion += grad * det_f(grad)
            result += diffusion / 4    # length(directions)
            mtx = result

        if result.min() < 0 or result.max() > 255:
            result = self.matrix_helper.normalize(result)

        return result

    def apply_bilateral_filter(self, mtx, sigma_s, sigma_r):
        mtx = self.matrix_helper.rgb2gray(mtx)
        filter = np.ones((3,3))
        h, w = mtx.shape

        padded_mtx = self.matrix_helper.pad_mtx(mtx, filter)
        result = np.empty_like(mtx)
        for i in range(h):
            for j in range(w):
                sub_mtx = padded_mtx[i:i+filter.shape[0], j:j+filter.shape[1], ...]
                adaptative_weight = self.get_bilateral_filter_weights(sub_mtx, sigma_s, sigma_r)
                result[i][j] = np.sum(np.multiply(sub_mtx, adaptative_weight))/np.sum(adaptative_weight)
        return result

    def get_bilateral_filter_weights(self, sub_mtx, sigma_s, sigma_r):
        weights = np.empty_like(sub_mtx)
        h, w = sub_mtx.shape
        sigma_s_sq = sigma_s**2
        sigma_r_sq = sigma_r**2
        for i in range(h):
            for j in range(w):
                dist_sq = (1-i)**2 + (1-j)**2
                dist_factor = -dist_sq/(2*sigma_s_sq)
                color_factor = -((sub_mtx[1][1] - sub_mtx[i][j])**2)/(2*sigma_r_sq)
                weights[i][j] = math.exp(dist_factor + color_factor)
        return weights