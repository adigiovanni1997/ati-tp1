import numpy as np
from scipy.stats import truncnorm
import matplotlib.pyplot as plt

class NoiseGenerator:
    def __init__(self, matrix_helper):
        self.matrix_helper = matrix_helper

    def get_truncated_normal(self, mean=0, sd=1, low=0, upp=10):
        return truncnorm(
            (low - mean) / sd, (upp - mean) / sd, loc=mean, scale=sd)

    def normal_dist(self, std, mean=0, size=None):
        return np.random.normal(loc=mean, scale=std, size=size)

    def rayleigh_dist(self, scale, size=None):
        return np.random.rayleigh(scale=scale, size=size)

    def exp_dist(self, scale, size=None):
        return np.random.exponential(scale=scale, size=size)

    def generate_noise_samples(self):
        normal_dist = self.normal_dist(std=15, size=100 * 100)
        normal_mtx = np.asarray(normal_dist).reshape(100, 100)
        normal_mtx = self.matrix_helper.normalize(normal_mtx)
        plt.hist(normal_mtx.flatten(), bins=255, label='Normal sigma=15')

        rayleigh_dist = self.rayleigh_dist(scale=50, size=100 * 100)
        rayleigh_mtx = np.asarray(rayleigh_dist).reshape(100, 100)
        rayleigh_mtx = self.matrix_helper.normalize(rayleigh_mtx)
        plt.hist(rayleigh_mtx.flatten(), bins=255, label='Rayleigh phi=50')
        plt.xlim(0, 255)

        exp_dist = self.exp_dist(scale=1, size=100 * 100)
        exp_mtx = np.asarray(128 * exp_dist).reshape(100, 100)
        exp_mtx = self.matrix_helper.normalize(exp_mtx)
        plt.hist(exp_mtx.flatten(), bins=255, label='Exponencial lambda=50')
        plt.legend()
        plt.show()

        ret = {}
        ret['Normal'] = normal_mtx
        ret['Rayleigh'] = rayleigh_mtx
        ret['Exp'] = exp_mtx
        return ret

    def apply_normal_noise(self, values, mtx):
        mtx = self.matrix_helper.rgb2gray(mtx).astype(np.uint8)
        pixelCount = mtx.shape[0] * mtx.shape[1]
        norm_dist = self.normal_dist(std=values['sigma'], size=pixelCount).reshape(mtx.shape)
        for i in range(mtx.shape[0]):
            for j in range(mtx.shape[1]):
                mtx[i][j] += norm_dist[i][j]

        return self.matrix_helper.normalize(mtx)

    def apply_rayleigh_noise(self, values, mtx):
        mtx = self.matrix_helper.rgb2gray(mtx).astype(np.uint8)
        pixelCount = mtx.shape[0] * mtx.shape[1]
        rayleigh_dist = self.rayleigh_dist(scale=values['phi'], size=pixelCount).reshape(mtx.shape)
        for i in range(mtx.shape[0]):
            for j in range(mtx.shape[1]):
                mtx[i][j] += rayleigh_dist[i][j]

        return self.matrix_helper.normalize(mtx)

    def apply_exp_noise(self, values, mtx):
        mtx = self.matrix_helper.rgb2gray(mtx).astype(np.uint8)
        pixelCount = mtx.shape[0] * mtx.shape[1]
        exp_dist = self.exp_dist(scale=values['lambda'], size=pixelCount).reshape(mtx.shape)
        for i in range(mtx.shape[0]):
            for j in range(mtx.shape[1]):
                mtx[i][j] *= exp_dist[i][j]

        return self.matrix_helper.normalize(mtx)

    def apply_salt_and_pepper_noise(self, mtx, p0):
        p1 = 1 - p0
        mtx = self.matrix_helper.rgb2gray(mtx).astype(np.uint8)
        pixelCount = mtx.shape[0] * mtx.shape[1]
        rand_dist = np.random.uniform(size=pixelCount).reshape(mtx.shape)
        for i in range(mtx.shape[0]):
            for j in range(mtx.shape[1]):
                if rand_dist[i][j] < p0:
                    mtx[i][j] = 0
                elif rand_dist[i][j] > p1:
                    mtx[i][j] = 255

        return mtx