from PyQt5.QtWidgets import QMainWindow, QHBoxLayout, QPushButton, QWidget, QVBoxLayout, QCheckBox, QDesktopWidget, \
    QDialog

from ImageViewer import ImageViewer


class SelectAreaWindow(QDialog):

    def __init__(self, parent, image, width, height, name='Secuencia'):
        super(SelectAreaWindow, self).__init__(parent)

        self.ret = {}
        self.start_button = None

        # Arbitrary values
        self.image = image
        self.width = width
        self.height = height
        self.imageViewerWidth = self.width
        self.imageViewerHeight = self.height
        if parent is not None:
            self.setGeometry((parent.width - self.width)/2, (parent.height - self.height)/2, self.width, self.height)
        else:
            center_point = QDesktopWidget().availableGeometry().center()
            self.setGeometry(0, 0, self.width + 150, self.height + 100)
            self.move(center_point)

        self.setWindowTitle(name)

        self.imageViewer = ImageViewer(self, self.width/2, self.height/2, self.imageViewerWidth, self.imageViewerHeight)
        self.imageViewer.setImage(image)
        self.menu = self.init_layout(self.imageViewer)
        self.show()

    def init_layout(self, viewer):
        menu = QHBoxLayout(self)
        self.start_button = QPushButton('Comenzar')
        self.start_button.clicked.connect(self.validate)
        menu.addWidget(self.start_button)
        h_widget = QWidget(self)
        h_widget.setLayout(menu)

        v_layout = QVBoxLayout(self)
        v_layout.addWidget(h_widget)
        v_layout.addWidget(viewer)
        widget = QWidget(self)
        widget.setLayout(v_layout)
        return widget

    def validate(self):
        if self.imageViewer.isAreaSelected:
            self.accept()

    def get_results(self):
        return self.imageViewer.get_selected_area()

    @staticmethod
    def getActions(img, width, height, name=None):
        dialog = SelectAreaWindow(None, image=img, width=width, height=height, name=name)
        result = dialog.exec_()
        results = dialog.get_results()
        return (results, result == QDialog.Accepted)