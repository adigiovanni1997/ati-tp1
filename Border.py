import math
import cv2 as cv
import numpy as np
import matplotlib.pyplot as plt

from Filter import Filter

class Border():
    def __init__(self, matrix_helper, filter_helper):
        self.matrix_helper = matrix_helper
        self.filter_helper = filter_helper
        self.g = np.array([[1,  4,  7,  4, 1],
                           [4, 16, 26, 16, 4],
                           [7, 26, 41, 26, 7],
                           [4, 16, 26, 16, 4],
                           [1,  4,  7,  4, 1]]) / 273

    def get_combiner(self, combiner):
        combiner_map = {
            'distancia':lambda grads: np.linalg.norm(grads),
            'promedio':lambda grads: np.average(np.asarray(grads)),
            'max': lambda grads: max(grads),
            'min': lambda grads: min(grads)
        }
        return combiner_map[combiner]

    def gradient_edge(self, mtx, combiner_string, filter, rotations=2):
        if np.ndim(filter) != 2 or filter.shape[0] != filter.shape[1]:
            pass

        combiner = self.get_combiner(combiner_string)

        if rotations == 2:
            filters = [filter, self.matrix_helper.rotate_clockwise(filter, 2)]
        elif rotations == 4:
            filters = [self.matrix_helper.rotate_clockwise(filter, r) for r in range(rotations)]
        elif rotations == 1:
            filters = [filter]
        else:
            pass

        h,w,d = mtx.shape
        padded_mtx = self.matrix_helper.pad_mtx(mtx, filter)
        result = np.zeros_like(mtx, dtype=np.float64)
        for channel in range(d):
            for i in range(h):
                for j in range(w):
                    sub_mtx = padded_mtx[i:i+filter.shape[0], j:j+filter.shape[1], channel, ...]
                    grads = [np.sum(np.multiply(sub_mtx, f)) for f in filters]
                    result[i][j][channel] = combiner(grads)

        return result

    def canny_edge(self, mtx, std=10):
        mtx = self.filter_helper.apply_gauss_filter(mtx, size=7, std=std)
        v_filter = np.array([[-1,-2,-1], [0, 0, 0], [1,2,1]])
        h_filter = self.matrix_helper.rotate_clockwise(v_filter, 2)
        v_gradient_mtx = self.gradient_edge(mtx, 'distancia', v_filter, rotations=1)
        h_gradient_mtx = self.gradient_edge(mtx, 'distancia', h_filter, rotations=1)

        directions_matrix = self.canny_edge_directions_matrix(v_gradient_mtx, h_gradient_mtx)
        gradient_mtx = self.gradient_edge(mtx, 'distancia', v_filter, rotations=2)

        gradient_mtx = self.eliminate_double_edges(gradient_mtx, directions_matrix)
        result = self.canny_threshold(gradient_mtx, 20, 80)
        return result

    def canny_threshold(self, gradient_mtx, t1, t2):
        width = gradient_mtx.shape[0]
        height = gradient_mtx.shape[1]
        channels = 1
        result = np.zeros((width, height))

        for k in range(channels):
            for i in range(height):
                for j in range(width):
                    if result[i][j] == 0:
                        if gradient_mtx[i][j][k] >= t2:
                            result[i][j] = 255

        aux_result = np.pad(result, 1, 'constant', constant_values=0)
        for k in range(channels):
            for i in range(height):
                for j in range(width):
                    if aux_result[i + 1][j + 1] == 0:
                        if t1 <= gradient_mtx[i][j][k] < t2:
                            if aux_result[i+1+1][j+1] + aux_result[i+1-1][j+1] + aux_result[i+1][j+1+1] + aux_result[i+1][j+1-1] > 0:
                                result[i][j] = 255

        return result

    def eliminate_double_edges(self, gradient_mtx, directions_matrix):
        result = np.copy(gradient_mtx)
        width = result.shape[0]
        height = result.shape[1]
        channels = 1
        for k in range(channels):
            for i in range(height):
                for j in range(width):
                    gradient = gradient_mtx[i][j][k]
                    if directions_matrix[i][j][k] == 0:
                        # horizontal
                        if (j>0 and gradient < gradient_mtx[i][j-1][k]) or (j<width-1 and gradient < gradient_mtx[i][j+1][k]):
                            result[i][j][k] = 0
                    if directions_matrix[i][j][k] == 1:
                        # top-right
                        if (j>0 and i<height-1 and gradient < gradient_mtx[i+1][j-1][k]) or (j<width-1 and i>0 and gradient < gradient_mtx[i-1][j+1][k]):
                            result[i][j][k] = 0
                    if directions_matrix[i][j][k] == 2:
                        # vertical
                        if (i>0 and gradient < gradient_mtx[i-1][j][k]) or (i<height-1 and gradient < gradient_mtx[i+1][j][k]):
                            result[i][j][k] = 0
                    if directions_matrix[i][j][k] == 3:
                        # top-left
                        if (j>0 and i>0 and gradient < gradient_mtx[i-1][j-1][k]) or (j<width-1 and i<height-1 and gradient < gradient_mtx[i+1][j+1][k]):
                            result[i][j][k] = 0
        return result

    def canny_edge_directions_matrix(self, v_gradient_mtx, h_gradient_mtx):
        result = np.zeros_like(v_gradient_mtx)
        width = result.shape[0]
        height = result.shape[1]
        channels = 1
        for k in range(channels):
            for i in range(height):
                for j in range(width):
                    gy = v_gradient_mtx[i][j][k]
                    gx = h_gradient_mtx[i][j][k]
                    if gx == 0:
                        angle = math.pi/2
                    else:
                        angle = math.atan(gy/gx)
                    result[i][j][k] = self.map_angle_to_region(angle)
        return result

    def map_angle_to_region(self, value):
        if value >= 0 and value < 0.3926991: # 22.5
            return 0
        if value >= 0.3926991 and value < 1.178097: #67.5
            return 1
        if value >= 1.178097 and value < 1.9634954: #112.5
            return 2
        if value >= 1.9634954 and value < 2.7488936: #157.5
            return 3
        if value >= 2.7488936 and value <= math.pi: #180
            return 0

    def hough_transform(self, mtx, e=1, candidate_count=4):
        bin_mtx = self.canny_edge(mtx)

        var_count = 500
        p_amp = math.sqrt(2)*max(mtx.shape)
        theta_step = math.pi/var_count
        theta_values = np.arange(-math.pi/2, math.pi/2, theta_step)
        p_step = 2*p_amp/var_count
        p_values = np.arange(-p_amp, p_amp, p_step)

        accum_mtx = np.zeros((var_count, var_count))

        w = mtx.shape[0]
        h = mtx.shape[1]

        cos_list = [math.cos(t) for t in theta_values]
        sin_list = [math.sin(t) for t in theta_values]

        for i in range(h):
            for j in range(w):
                if bin_mtx[i][j] == 0:
                    continue

                x = j
                y = h - i
                for k in range(var_count):
                    cos = cos_list[k]
                    sin = sin_list[k]
                    p_computed = x*cos + y*sin
                    for l in range(var_count):
                        p = p_values[l]
                        if abs(p - p_computed) < e:
                            accum_mtx[k][l] += 1

        candidates = []
        for c in range(candidate_count):
            max_arg = np.argmax(accum_mtx)
            row = max_arg//var_count
            column = max_arg%var_count

            theta_index = row
            p_index = column
            candidates.append((p_values[p_index], theta_values[theta_index], accum_mtx[row][column]))
            accum_mtx[row][column] = 0

        ret = bin_mtx.copy()
        for c in candidates:
            ret = self.plot_line(ret, c[0], c[1])

        return np.dstack([ret, bin_mtx, np.zeros_like(ret)])

    def plot_line(self, mtx, p, theta):
        cos = math.cos(theta)
        sin = math.sin(theta)

        # y = (p - x.cos)/sin

        w = mtx.shape[0]
        h = mtx.shape[1]

        if abs(sin) <= 0.15:
            # vertical line
            x = int(abs(p//cos))
            if 0 <= x < w:
                for i in range(h):
                    mtx[i][x] = 255
            return mtx

        for x in range(w):
            y = (p-x*cos)/sin

            i = math.floor(h - y)
            j = x

            if 0 <= i < h:
                mtx[i][j] = 255

        return mtx

    def dist(self, a, b, amp0, amp1):
        return abs(a[0] - b[0])/amp0 + abs(a[1] - b[1])/amp1

    def susan(self, mtx, t=27):
        filter = np.ones((7,7))
        filter[0] = np.array([0, 0, 1, 1, 1, 0, 0])
        filter[1] = np.array([0, 1, 1, 1, 1, 1, 0])
        filter[5] = np.array([0, 1, 1, 1, 1, 1, 0])
        filter[6] = np.array([0, 0, 1, 1, 1, 0, 0])

        mtx = self.matrix_helper.rgb2gray(mtx)
        pad = 3
        padded_mtx = np.pad(mtx, ((pad, pad), (pad, pad)), 'constant', constant_values=0.5)

        width = mtx.shape[0]
        height = mtx.shape[1]

        edge_mtx = np.zeros_like(mtx)
        corner_mtx = np.zeros_like(mtx)

        for i in range(height):
            for j in range(width):
                sub_mtx = padded_mtx[i:i + filter.shape[0], j:j + filter.shape[1]]

                center = sub_mtx[3][3]
                count = 0
                N = 0
                for k in range(7):
                    for l in range(7):
                        if filter[k][l] == 0:
                            continue
                        if sub_mtx[k][l] == 0.5:
                            continue

                        N += 1
                        if abs(center - sub_mtx[k][l]) <= t:
                            count += 1

                if N == 0:
                    continue
                s = 1 - count/N

                if 0.20 <= s < 0.6:
                    edge_mtx[i][j] = 255
                if 0.6 <= s:
                    corner_mtx[i][j] = 255

        return np.dstack([mtx, corner_mtx, edge_mtx])



    def active_contour(self, mtx, phi_mtx, Na=80, Ng=20, max_iters=None):
        inside_mean = np.mean(mtx[phi_mtx == -3], axis=0)
        outside_mean = np.mean(mtx[phi_mtx == 3], axis=0)
        iters = 0
        count = 0

        while max_iters is None or iters < max_iters:
            for _ in range(Na):
                iters += 1
                count = 0

                lout = phi_mtx == 1
                Fd = self.Fd_speed(mtx[lout], inside_mean, outside_mean)

                need_to_switch = np.transpose(np.nonzero(lout))[Fd > 0]
                count += need_to_switch.shape[0]

                self.switch(need_to_switch, phi_mtx, 3, 1)      # in

                lin = phi_mtx == -1

                self.clean_in(np.transpose(np.nonzero(lin)), phi_mtx)

                lin = phi_mtx == -1

                Fd = self.Fd_speed(mtx[lin], inside_mean, outside_mean)

                need_to_switch = np.transpose(np.nonzero(lin))[Fd < 0]
                count += need_to_switch.shape[0]

                self.switch(need_to_switch, phi_mtx, -3, -1)    # out

                lout = phi_mtx == 1

                self.clean_out(np.transpose(np.nonzero(lout)), phi_mtx)

                if count == 0:
                    break

            for _ in range(Ng):
                iters += 1
                lout = phi_mtx == 1

                indexes = np.transpose(np.nonzero(lout))
                g = self.compute_g(indexes, phi_mtx)
                self.switch(indexes[g < 0], phi_mtx, 3, 1)

                lin = phi_mtx == -1

                self.clean_in(np.transpose(np.nonzero(lin)), phi_mtx)

                lin = phi_mtx == -1

                indexes = np.transpose(np.nonzero(lin))
                g = self.compute_g(indexes, phi_mtx)
                self.switch(indexes[g < 0], phi_mtx, 3, 1)

                lout = phi_mtx == 1

                self.clean_out(np.transpose(np.nonzero(lout)), phi_mtx)

            if count == 0:
                break

        return phi_mtx

    def Fd_speed(self, mtx, inside_mean, outside_mean):
        dist_in = mtx - inside_mean
        dist_out = mtx - outside_mean
        return np.sum(dist_out*dist_out, axis=1) - np.sum(dist_in*dist_in, axis=1)

    def neighbors(self, indexes, phi_mtx):
        n = indexes[:, 0] - 1
        s = indexes[:, 0] + 1
        e = indexes[:, 1] - 1
        w = indexes[:, 1] + 1

        n = (n[0 <= n], indexes[0 <= n, 1])
        s = (s[s < phi_mtx.shape[0]], indexes[s < phi_mtx.shape[0], 1])
        e = (indexes[0 <= e, 0], e[0 <= e])
        w = (indexes[w < phi_mtx.shape[1], 0], w[w < phi_mtx.shape[1]])

        return n, s, e, w

    def neighbors_unbound(self, indexes):
        n = indexes[:, 0]
        s = indexes[:, 0] + 2
        e = indexes[:, 1]
        w = indexes[:, 1] + 2

        n = (n, indexes[:, 1] + 1)
        s = (s, indexes[:, 1] + 1)
        e = (indexes[:, 0] + 1, e)
        w = (indexes[:, 0] + 1, w)

        return n, s, e, w

    def switch(self, indexes, phi_mtx, region_val, edge_val):
        phi_mtx[indexes[:, 0], indexes[:, 1]] = -edge_val

        n, s, e, w = self.neighbors(indexes, phi_mtx)

        n1 = phi_mtx[n] == region_val
        phi_mtx[n[0][n1], n[1][n1]] = edge_val
        n2 = phi_mtx[s] == region_val
        phi_mtx[s[0][n2], s[1][n2]] = edge_val
        n3 = phi_mtx[e] == region_val
        phi_mtx[e[0][n3], e[1][n3]] = edge_val
        n4 = phi_mtx[w] == region_val
        phi_mtx[w[0][n4], w[1][n4]] = edge_val

    def clean_in(self, indexes, phi_mtx):
        padded_mtx = np.pad(phi_mtx, ((1, 1), (1, 1)), mode='constant')
        n, s, e, w = self.neighbors_unbound(indexes)

        inner = np.logical_and(np.logical_and(padded_mtx[n] <= 0, padded_mtx[s] <= 0),
                               np.logical_and(padded_mtx[e] <= 0, padded_mtx[w] <= 0))

        to_clean = indexes[inner]
        if to_clean.size > 0:
            phi_mtx[to_clean[:, 0], to_clean[:, 1]] = -3

    def clean_out(self, indexes, phi_mtx):
        padded_mtx = np.pad(phi_mtx, ((1, 1), (1, 1)), mode='constant')
        n, s, e, w = self.neighbors_unbound(indexes)

        outer = np.logical_and(np.logical_and(padded_mtx[n] >= 0, padded_mtx[s] >= 0),
                               np.logical_and(padded_mtx[e] >= 0, padded_mtx[w] >= 0))

        to_clean = indexes[outer]
        if to_clean.size > 0:
            phi_mtx[to_clean[:, 0], to_clean[:, 1]] = 3

    def compute_g(self, indexes, phi_mtx):
        padded_mtx = np.pad(phi_mtx, ((2, 2), (2, 2)), mode='constant')
        g_values = np.empty((indexes.shape[0]), dtype=np.float64)

        for i in range(indexes.shape[0]):
            x = indexes[i][0]
            y = indexes[i][1]
            g_values[i] = np.sum(padded_mtx[x:x+5, y:y+5, ...] * self.g, axis=(0, 1))

        return g_values

    def generate_initial_phi_mtx(self, mtx, from_x, from_y, to_x, to_y):
        phi_mtx = mtx[..., 0].astype(np.int8)   # Puede que explote si no es rgb
        for j in range(phi_mtx.shape[0]):
            for i in range(phi_mtx.shape[1]):
                if ((i == from_x or i == to_x) and from_y <= j <= to_y) \
                        or (j == from_y or j == to_y) and from_x <= i <= to_x:
                    phi_mtx[j, i] = 1
                elif ((i == from_x+1 or i == to_x-1) and from_y+1 <= j <= to_y-1) \
                        or (j == from_y+1 or j == to_y-1) and from_x+1 <= i <= to_x-1:
                    phi_mtx[j, i] = -1
                elif from_x+2 <= i <= to_x-2 and from_y+2 <= j <= to_y-2:
                    phi_mtx[j, i] = -3
                else:
                    phi_mtx[j, i] = 3

        return phi_mtx

    def get_harris_combiner(self, k=0.04, combiner_string='cim1'):
        combiner_map = {
            'cim1': lambda Ix2, Iy2, Ixy: (Ix2*Iy2 - Ixy*Ixy) - k*(Ix2 + Iy2)*(Ix2 + Iy2),
            'cim2': lambda Ix2, Iy2, Ixy: (Ix2*Iy2 - Ixy*Ixy)/(Ix2 + Iy2 + 1e-6),
            'cim3': lambda Ix2, Iy2, Ixy: (Ix2*Iy2 - Ixy**4) - k*(Ix2 + Iy2)*(Ix2 + Iy2),
        }
        return combiner_map[combiner_string]

    def harris_detector(self, mtx, k=0.04, harris_combiner='cim1', point_count=250, tolerance=10):
        mtx = mtx.astype(np.float64)
        v_filter = np.array([[-1, -2, -1], [0, 0, 0], [1, 2, 1]])
        I_y = self.gradient_edge(mtx, 'distancia', v_filter, rotations=1)
        h_filter = self.matrix_helper.rotate_clockwise(v_filter, steps=2)
        I_x = self.gradient_edge(mtx, 'distancia', h_filter, rotations=1)

        I_x2 = np.multiply(I_x, I_x)
        I_y2 = np.multiply(I_y, I_y)
        I_xy = np.multiply(I_x, I_y)

        I_x2 = self.filter_helper.apply_gauss_filter(I_x2, 7, std=2)
        I_y2 = self.filter_helper.apply_gauss_filter(I_y2, 7, std=2)
        I_xy = self.filter_helper.apply_gauss_filter(I_xy, 7, std=2)

        combiner = self.get_harris_combiner(k, harris_combiner)

        result = combiner(I_x2, I_y2, I_xy)

        ret = np.zeros_like(result)

        # for c in range(point_count):
        #     max_arg = np.argmax(result)
        #     i = max_arg//mtx.shape[0]
        #     j = max_arg%mtx.shape[0]
        #     result[i][j] = 0
        #     ret[i][j] = 255

        ret[result > result.max() / tolerance] = 255

        ret = np.max(ret, axis=2)
        mtx = self.matrix_helper.rgb2gray(mtx)
        return np.dstack([mtx, ret, np.zeros_like(mtx)])

    def sift(self, img1, img2, max_matches=100, random=False):
        # Initiate SIFT detector
        sift = cv.xfeatures2d.SIFT_create()

        # find the keypoints and descriptors with SIFT
        kp1, des1 = sift.detectAndCompute(img1, None)
        kp2, des2 = sift.detectAndCompute(img2, None)

        # BFMatcher with default params
        bf = cv.BFMatcher()
        matches = bf.knnMatch(des1, des2, k=2)

        # Apply ratio test
        good = []
        for m, n in matches:
            if m.distance < 0.75 * n.distance:
                good.append([m])

        if len(good) > max_matches:
            if random:
                good = np.array(good)[np.random.choice(len(good), size=max_matches, replace=False)]
            else:
                good = sorted(good, key = lambda x:x[0].distance)[:max_matches]

        # cv.drawMatchesKnn expects list of lists as matches.
        img3 = cv.drawMatchesKnn(img1, kp1, img2, kp2, good, None, flags=cv.DrawMatchesFlags_NOT_DRAW_SINGLE_POINTS)

        return img3