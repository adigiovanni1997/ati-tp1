from PyQt5.QtWidgets import QMainWindow, QHBoxLayout, QPushButton, QWidget, QVBoxLayout, QCheckBox, QDesktopWidget, \
    QDialog

from ImageViewer import ImageViewer


class ResultWindow(QDialog):

    def __init__(self, parent, image, width, height, name='Resultado'):
        super(ResultWindow, self).__init__(parent)

        self.ret = {}

        # Arbitrary values
        self.image = image
        self.width = width
        self.height = height
        self.imageViewerWidth = self.width
        self.imageViewerHeight = self.height
        if parent is not None:
            self.setGeometry((parent.width - self.width)/2, (parent.height - self.height)/2, self.width, self.height)
        else:
            center_point = QDesktopWidget().availableGeometry().center()
            self.setGeometry(0, 0, self.width + 150, self.height + 100)
            self.move(center_point)

        self.setWindowTitle(name)

        self.imageViewer = ImageViewer(self, self.width/2, self.height/2, self.imageViewerWidth, self.imageViewerHeight)
        self.imageViewer.setImageScaled(image, self.imageViewerWidth, self.imageViewerWidth)
        self.menu = self.init_layout(self.imageViewer)
        self.show()

    def init_layout(self, viewer):
        menu = QHBoxLayout(self)
        self.left = QCheckBox('Copiar izquierda')
        self.right = QCheckBox('Copiar derecha')
        finish_button = QPushButton('Actualizar')
        finish_button.clicked.connect(self.accept)
        menu.addWidget(self.left)
        menu.addWidget(self.right)
        menu.addWidget(finish_button)
        h_widget = QWidget(self)
        h_widget.setLayout(menu)

        v_layout = QVBoxLayout(self)
        v_layout.addWidget(h_widget)
        v_layout.addWidget(viewer)
        widget = QWidget(self)
        widget.setLayout(v_layout)
        return widget

    def get_results(self):
        return {'l': self.left.isChecked(), 'r': self.right.isChecked()}

    @staticmethod
    def getActions(img, width, height, name=None):
        dialog = ResultWindow(None, image=img, width=width, height=height, name=name)
        result = dialog.exec_()
        results = dialog.get_results()
        return (results, result == QDialog.Accepted)


