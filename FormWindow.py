import sys
from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *


class FormWindow(QDialog):
    def __init__(self, parent, title, fields, valid_values, field_order):
        super(FormWindow, self).__init__(parent)

        self.values = {}
        self.fields = {}
        self.types = fields

        layout = QFormLayout()

        field_names = field_order if field_order is not None else fields.keys()

        for field_name in field_names:
            field_type = fields[field_name]

            label = QLabel()
            label.setText(field_name)

            if field_type == 'float':
                field = QLineEdit()
                field.setValidator(QDoubleValidator(0, 100, 2))
            elif field_type == 'int':
                field = QLineEdit()
                field.setValidator(QIntValidator(0, 255))
            elif field_type == 'combo':
                field = QComboBox()
                field.addItems(valid_values[field_name])

            layout.addRow(label, field)
            self.fields[field_name] = field

        label = QLabel()
        label.setText('Pisar imagen')
        self.overwrite_viewer_button = QRadioButton() 
        layout.addRow(label, self.overwrite_viewer_button)

        acc_btn = QPushButton("Aceptar", self)
        acc_btn.setDefault(True)
        acc_btn.clicked.connect(self.parse_input)
        rej_btn = QPushButton("Cancelar", self)
        rej_btn.clicked.connect(self.reject)

        layout.addRow(acc_btn, rej_btn)

        self.setLayout(layout)
        self.setWindowTitle(title)

    def parse_input(self):
        self.values['overwrite'] = self.overwrite_viewer_button.isChecked()
        for field_name, field in self.fields.items():
            if self.types[field_name] == 'float' and field.hasAcceptableInput():
                self.values[field_name] = float(field.text())
            elif self.types[field_name] == 'int' and field.hasAcceptableInput():
                self.values[field_name] = int(field.text())
            elif self.types[field_name] == 'combo':
                self.values[field_name] = field.currentText()
            else:
                self.reject()
                return

        self.accept()

    def text_changed(self):
        sender = self.sender()
        if not sender.hasAcceptableInput():
            sender.undo()

    @classmethod
    def get_values(cls, parent, title, fields, valid_values=None, field_order=None):
        dialog = cls(parent, title, fields, valid_values, field_order)
        dialog.exec()
        return dialog.values, dialog.result() == QDialog.Accepted

