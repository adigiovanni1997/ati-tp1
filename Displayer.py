from PyQt5.QtWidgets import QColorDialog

from ImageViewer import ImageViewer
from ResultWindow import ResultWindow
from PyQt5.QtGui import QPixmap, QImage, QColor
import numpy as np


class Displayer():
    def __init__(self):
        self.viewers = {}

    def add_viewer(self, viewer, ref):
        self.viewers[ref] = viewer

    def get_viewer(self, ref):
        return self.viewers[ref]

    def copy(self, origin, target):
        origin_viewer = self.get_viewer(origin)
        img = origin_viewer.image
        self.set_img(target, img=img)

    def copy_selection(self, origin, target):
        origin_viewer = self.get_viewer(origin)
        if origin_viewer.isAreaSelected:
            img = origin_viewer.get_selected_pixmap().toImage()
            origin_viewer.resetSelectedArea()
            self.set_img(target, img=img)

    def paint_selection(self, ref):
        viewer = self.viewers[ref]
        if viewer.isAreaSelected:
            color = QColorDialog.getColor()
            viewer.paint_area(viewer.get_selected_area(), color)
            viewer.resetSelectedArea()

    def image_to_matrix(self, ref):
        viewer = self.viewers[ref]
        return self.get_matrix(viewer)

    def get_matrix(self, viewer):
        img = viewer.image.convertToFormat(QImage.Format_RGB32)

        width = img.width()
        height = img.height()

        ptr = img.bits()
        ptr.setsize(height * width * 4)
        data = np.frombuffer(ptr, np.uint8).reshape(height, width, 4)

        arr = np.empty((height, width, 3), np.uint8, 'C')

        # image is BGRA
        arr[..., 0] = data[..., 2]
        arr[..., 1] = data[..., 1]
        arr[..., 2] = data[..., 0]
        return arr

    def set_img(self, ref, mtx=None, img=None):
        viewer = self.viewers[ref]
        if mtx is not None:
            img = self.matrix_to_image(mtx)
        viewer.setImage(img)

    def matrix_to_image(self, mtx):
        mtx = mtx.astype(np.uint8)

        if np.ndim(mtx) == 2:
            # get the shape of the array
            height, width = np.shape(mtx)

            # calculate the total number of bytes in the frame
            totalBytes = mtx.nbytes

            # divide by the number of rows
            bytesPerLine = int(totalBytes / height)

            return QImage(mtx, mtx.shape[1], mtx.shape[0], bytesPerLine, QImage.Format_Grayscale8)

        elif np.ndim(mtx) == 3:
            data = np.empty((mtx.shape[0], mtx.shape[1], 4), np.uint8, 'C')
            data[..., 0] = mtx[..., 2]
            data[..., 1] = mtx[..., 1]
            data[..., 2] = mtx[..., 0]
            if mtx.shape[2] == 3:
                data[..., 3].fill(255)
                img_format = QImage.Format_RGB32
            else:
                data[..., 3] = mtx[..., 3]
                img_format = QImage.Format_ARGB32

            return QImage(data, mtx.shape[1], mtx.shape[0], img_format)
        else:
            raise ValueError("Can't display result, wrong dimensions.")

    def image_to_matrix_lucas(self, image):
        img = image.convertToFormat(QImage.Format_RGB32)

        width = img.width()
        height = img.height()

        ptr = img.bits()
        ptr.setsize(height * width * 4)
        data = np.frombuffer(ptr, np.uint8).reshape(height, width, 4)

        arr = np.empty((height, width, 3), np.uint8, 'C')

        # image is BGRA
        arr[..., 0] = data[..., 2]
        arr[..., 1] = data[..., 1]
        arr[..., 2] = data[..., 0]
        return arr

    def display_mtx(self, mtx, name=None):
        img = self.matrix_to_image(mtx)
        ret, ok = ResultWindow.getActions(img, mtx.shape[0], mtx.shape[1], name)
        if ok:
            for x in ret:
                if ret[x]:
                    self.set_img(x, mtx=mtx)
