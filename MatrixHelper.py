import numpy as np
import math

class MatrixHelper:
    def __init__(self):
        pass

    def rotate_clockwise(self, mtx, steps):
        if steps == 0:
            return mtx

        mtx = np.copy(mtx)
        mem = mtx[0][0]
        mtx[0][0] = mtx[1][0]
        mtx[1][0] = mtx[2][0]
        mtx[2][0] = mtx[2][1]
        mtx[2][1] = mtx[2][2]
        mtx[2][2] = mtx[1][2]
        mtx[1][2] = mtx[0][2]
        mtx[0][2] = mtx[0][1]
        mtx[0][1] = mem
        return self.rotate_clockwise(mtx, steps-1)

    def equalize(self, mtx):
        mtx = self.rgb2gray(mtx).astype(np.uint8)
        pixel_count = mtx.shape[0] * mtx.shape[1]
        hist = np.histogram(mtx.flatten(), bins=255)
        t_mtx = np.asarray([int(255 * np.sum(hist[0][:g]) / pixel_count) for g in range(0, 255)])

        for i in range(0, mtx.shape[0]):
            for j in range(0, mtx.shape[1]):
                mtx[i][j] = t_mtx[mtx[i][j]]

        return mtx

    def pad_mtx(self, mtx, filter, v=0):
        pad = (filter.shape[0] - 1) // 2

        if np.ndim(mtx) == 2:
            padded_mtx = np.pad(mtx, ((pad, pad), (pad, pad)), 'constant', constant_values=v)
        else:
            padded_mtx = np.pad(mtx, ((pad, pad), (pad, pad), (0, 0)), 'constant', constant_values=v)

        return padded_mtx

    def contrast(self, mtx, r1, r2):
        if r1 >= r2:
            return

        mtx[mtx <= r1] = mtx[mtx <= r1] / 2

        mid_tones = np.logical_and(r1 < mtx, mtx <= r2)
        mtx[mid_tones] = r1/2 + (mtx[mid_tones] - r1) * ((255 - (255 - r2)/2 - r1/2) / (r2 - r1))

        mtx[r2 < mtx] = 255 - (255 - mtx[r2 < mtx]) / 2

        return mtx

    def product(self, scalar, mtx):
        mtx = mtx.astype(np.float)
        mtx *= scalar
        return self.dynamic_range(mtx)

    def normalize(self, mtx):
        mtx = mtx.astype(np.float64)
        mtx -= mtx.min()
        if mtx.max() > 0:
            mtx *= 255 / mtx.max()
        return mtx

    def dynamic_range(self, mtx):
        mtx = mtx.astype(np.float64)
        if mtx.min() < 0:
            mtx -= mtx.min()

        c = 255.0 / math.log(1 + mtx.max())

        mtx = c * np.log(1 + mtx)
        return mtx

    def gamma_exp(self, mtx, gamma):
        mtx = mtx.astype(np.float64)
        if mtx.min() < 0:
            mtx -= mtx.min()

        c = math.pow(255.0, 1 - gamma)

        mtx = c * np.power(mtx, gamma)
        return mtx

    def rgb2gray(self, rgb_mtx):
        return np.dot(rgb_mtx[..., :3], [0.2989, 0.5870, 0.1140])