from PyQt5.QtWidgets import QMainWindow, QHBoxLayout, QPushButton, QWidget, QVBoxLayout, QCheckBox, QDesktopWidget, QDialog
from PyQt5.QtGui import QImage

from Border import Border
from Filter import Filter
from ImageViewer import ImageViewer
import numpy as np

from MatrixHelper import MatrixHelper


class SequenceWindow(QDialog):
    def __init__(self, parent, sequence, phi_mtx, width, height, name='Secuencia'):
        super(SequenceWindow, self).__init__(parent)

        self.ret = {}
        self.start_button = None

        # Arbitrary values
        self.sequence = sequence
        self.phi_mtx = phi_mtx
        self.index = 0
        self.width = width
        self.height = height
        self.imageViewerWidth = self.width
        self.imageViewerHeight = self.height
        if parent is not None:
            self.setGeometry((parent.width - self.width)/2, (parent.height - self.height)/2, self.width, self.height)
        else:
            center_point = QDesktopWidget().availableGeometry().center()
            self.setGeometry(0, 0, self.width + 150, self.height + 100)
            self.move(center_point)

        self.setWindowTitle(name)

        self.matrix_helper = MatrixHelper()
        self.filter_helper = Filter(self.matrix_helper)
        self.border_helper = Border(self.matrix_helper, self.filter_helper)

        self.imageViewer = ImageViewer(self, self.width/2, self.height/2, self.imageViewerWidth, self.imageViewerHeight)
        self.updateImage(sequence[0])
        self.menu = self.init_layout(self.imageViewer)
        self.show()

    def init_layout(self, viewer):
        menu = QHBoxLayout(self)
        self.start_button = QPushButton('Siguiente')
        self.start_button.clicked.connect(self.nextImage)
        menu.addWidget(self.start_button)
        h_widget = QWidget(self)
        h_widget.setLayout(menu)

        v_layout = QVBoxLayout(self)
        v_layout.addWidget(h_widget)
        v_layout.addWidget(viewer)
        widget = QWidget(self)
        widget.setLayout(v_layout)
        return widget

    def nextImage(self):
        if self.index == len(self.sequence):
            self.accept()
        else:
            self.process()
            self.index += 1

    def process(self):
        mtx = self.sequence[self.index]
        self.phi_mtx = self.border_helper.active_contour(mtx, self.phi_mtx)

        self.updateImage(mtx)

    def updateImage(self, mtx):
        mtx[self.phi_mtx == 1] = [255, 0, 0]
        mtx[self.phi_mtx == -1] = [0, 0, 255]

        self.imageViewer.setImage(self.matrix_to_image(mtx))

    def get_results(self):
        return True

    @staticmethod
    def start(sequence, phi_mtx, width, height, name=None):
        dialog = SequenceWindow(None, sequence=sequence, phi_mtx=phi_mtx, width=width, height=height, name=name)
        result = dialog.exec_()
        results = dialog.get_results()
        return (results, result == QDialog.Accepted)

    def matrix_to_image(self, mtx):
        mtx = mtx.astype(np.uint8)

        if np.ndim(mtx) == 2:
            # get the shape of the array
            height, width = np.shape(mtx)

            # calculate the total number of bytes in the frame
            totalBytes = mtx.nbytes

            # divide by the number of rows
            bytesPerLine = int(totalBytes / height)

            return QImage(mtx, mtx.shape[1], mtx.shape[0], bytesPerLine, QImage.Format_Grayscale8)

        elif np.ndim(mtx) == 3:
            data = np.empty((mtx.shape[0], mtx.shape[1], 4), np.uint8, 'C')
            data[..., 0] = mtx[..., 2]
            data[..., 1] = mtx[..., 1]
            data[..., 2] = mtx[..., 0]
            if mtx.shape[2] == 3:
                data[..., 3].fill(255)
                img_format = QImage.Format_RGB32
            else:
                data[..., 3] = mtx[..., 3]
                img_format = QImage.Format_ARGB32

            return QImage(data, mtx.shape[1], mtx.shape[0], img_format)
        else:
            raise ValueError("Can't display result, wrong dimensions.")